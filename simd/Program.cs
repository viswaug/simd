﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OSGeo.GDAL;
using OSGeo.OGR;
using OSGeo.OSR;
using System.Numerics;
using System.Diagnostics;
using System.IO;

namespace simd
{
    class Program
    {
        static void Main(string[] args)
        {
            string rasterFile = args[0];
            if(!File.Exists(rasterFile))
            {
                Console.WriteLine(string.Format("{0} is not a valid file", rasterFile));
                return;
            }
            GdalConfiguration.ConfigureGdal();
            GdalConfiguration.ConfigureOgr();
            using (var tif = Gdal.Open(rasterFile, Access.GA_ReadOnly))
            {
                using (var band = tif.GetRasterBand(1))
                {
                    Console.WriteLine(string.Format("Raster X size: {0}", tif.RasterXSize));
                    Console.WriteLine(string.Format("Raster Y size: {0}", tif.RasterYSize));
                    Console.WriteLine(string.Format("Raster cell size: {0}", tif.RasterXSize * tif.RasterYSize));
                    var vals = new short[tif.RasterXSize * tif.RasterYSize];
                    band.ReadRaster(0, 0, tif.RasterXSize, tif.RasterYSize, vals, tif.RasterXSize, tif.RasterYSize, 0, 0);

                    var watch1 = new Stopwatch();
                    watch1.Start();
                    var total = vals.Aggregate(0, (a, i) => a + i);
                    watch1.Stop();
                    Console.WriteLine(string.Format("Total is: {0}", total));
                    Console.WriteLine(string.Format("Time taken: {0}", watch1.ElapsedMilliseconds));

                    Console.WriteLine(string.Format("SIMD accelerated: {0}", Vector.IsHardwareAccelerated));
                    var watch2 = new Stopwatch();
                    watch2.Start();
                    int sum = GetSIMDVectorSum(vals);
                    watch2.Stop();
                    Console.WriteLine(string.Format("Another Total is: {0}", sum));
                    Console.WriteLine(string.Format("Time taken: {0}", watch2.ElapsedMilliseconds));
                }
            }
        }

        private static int GetSIMDVectorSum(short[] source)
        {
            int vecCount = Vector<int>.Count;
            int i = 0;
            int len = source.Length;
            Vector<int> temp = Vector<int>.Zero;
            for (i = 0; i + vecCount < len; i = i + vecCount)
            {
                var items = new int[vecCount];
                for (int k = 0; k < vecCount; k++)
                {
                    items[k] = source[i + k];
                }
                temp += new Vector<int>(items);
            }
            var remaining = new int[vecCount];
            for (int j = i, k =0; j < len; j++, k++)
            {
                remaining[k] = source[j];
            }
            temp += new Vector<int>(remaining);
            return Vector.Dot<int>(temp, Vector<int>.One);
        }
    }
}
